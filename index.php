<?php
/**
 * ====================
 * AVATAR-GENERATOR
 * ====================
 *
 * http://my-site/my-path/index.php?seed=your-id&theme=cat
 * theme is not required. You can replace cat by bird.
 *
 * @authors: Andreas Gohr, David Revoy, Jean-Pierre Pourrez (bazooka07)
 * @date: 2020-05-06
 *
 * This PHP is licensed under the short and simple permissive:
 * [MIT License](https://en.wikipedia.org/wiki/MIT_License)
 *
**/

// /!\ change the path to your system's cache or a folder(write permission)
// Note: this path end with / and is relative to the cat-avatar-generator.php file.

const CACHE_PATH = 'cache/';
const LIFETIME = 18144000; # (1 week = 604800, 1 day = 86400, 30 days = 18144000, 365 days = 31536000) in seconds

const PARTS = array(
	'cat'	=> array(
        'body'			=> 15,
        'fur'			=> 10,
        'eyes'			=> 15,
        'mouth'			=> 10,
        'accessorie'	=> 20,
    ),
    'bird'	=> array(
		'body'			=> 9,
		'hoop'			=> 9,
		'tail'			=> 9,
		'wing'			=> 9,
		'eyes'			=> 9,
		'bec'			=> 9,
		'accessorie'	=> 19,
    )
);

function create_avatar($seed, $theme) {

	$dir1 = CACHE_PATH . $theme;
	if(!is_writable(CACHE_PATH) or (!is_dir($dir1) and !mkdir($dir1) or !is_writable($dir1))) {
		header('HTTP/1.0 500 unwritable ' . basename($dir1) . ' cache');
		exit;
	}

    // clean up deprecated files from cache
    $now = time();
    foreach(glob($dir1 . '/*.png', GLOB_NOSORT) as $filename) {
		if($now - filemtime($filename) > LIFETIME) {
			unlink($filename);
		}
	}

    $result = @imagecreatetruecolor(70, 70)
        or die("GD image create failed");

	$transparent = imagecolorallocate($result, 0, 0, 0);
	imagecolortransparent($result, $transparent);

    // add parts
    foreach(PARTS[$theme] as $part => $num) {
        $file = __DIR__ . '/avatars/' . $theme . '/' . $part . '_' . rand(1, $num) . '.png';

        $im = @imagecreatefrompng($file);
        if(!$im) {
			header('HTTP/1.0 500 Failed to load ' . basename($file) . ' cache');
			exit;
		}

        imageSaveAlpha($im, true);
        imagecopy($result, $im, 0, 0, 0, 0, 70, 70);
        imagedestroy($im);
    }

	imagepng($result, $dir1 . '/' . $seed . '.png');
	imagedestroy($result);
}

if(!empty($_GET)) {
	$hash1 = filter_input(INPUT_GET, 'seed', FILTER_SANITIZE_STRING);

	if(empty($hash1)) {
		header('HTTP/1.0 404 Not Found');
		exit;
	}

	if(preg_match('@^[0-9a-f]{32}$@', $hash1) == 0) {
		// Not really a hash. Create it !
		$hash1 = preg_replace('/[^\w\.-]/', '', strtolower($hash1));
		$hash1 = md5($hash1);
	}

	$theme = filter_input(INPUT_GET, 'theme', FILTER_SANITIZE_STRING);
	if(empty($theme) or !array_key_exists($theme, PARTS)) {
		// Unknown theme. Choose one randomly
		$theme = array_rand(PARTS);
	}

	$filename = CACHE_PATH . $theme . '/' . $hash1 . '.png';

	if(!file_exists($filename) or time() - filemtime($filename) > LIFETIME) {
		// Avatar doesn't exist or is too older than LIFETIME
		create_avatar($hash1, $theme);
	}

	// Serve from the cache
	touch($filename);
	header('Pragma: public');
	header('Cache-Control: max-age=' . LIFETIME);
	header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + LIFETIME));
	header('Content-Type: image/png');
	readfile($filename);

	exit;
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="keywords" content="avatar,gravatar,identicon,clip-art" />
	<meta name="description" content="get your avatarwith your name, e-mail, phone, id-address, .." />
	<title>Get your avatar</title>
	<style type="text/css">
		body { background-color: #369; padding: 50vh 0 0; }
		form { max-width: 36rem; margin: 0 auto; padding: 0.3rem; background: #fff; transform: translateY(-50%); }
		form > div { margin: 0.3rem; }
		form > div:not(:last-of-type) { display: grid; grid-template-columns: 15rem 1fr; column-gap: 0.75rem; }
		form p { text-align: center; }
		form > p { margin: 0.3rem 0; }
		label { text-align: end; }
	</style>
</head><body>
	<form>
		<div>
			<label for="id_seed">Your name, mail, phone, ...</label>
			<input name="seed" id="id_seed" />
		</div>
		<div>
			<label for="id_theme">Theme</label>
			<select name="theme" id="id_theme">
<?php
	foreach(PARTS as $k=>$v) {
?>
				<option><?= $k ?></option>
<?php
	}
?>
			</select>
		</div>
		<div>
			<p><input type="submit" /></p>
		</div>
		<p><small>
			<em>Clip-arts are from <a href="https://www.davidrevoy.com" target="_blank">David Revoy</a></em><br />
			Get source from <a href="https://framagit.org/bazooka07/cat-avatar-generator.git">Framagit</a>
		</small></p>
	</form>
</body></html>
